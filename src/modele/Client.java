package modele;
public class Client {
    private int numero;
    private String raisonSoc;
    private String adresse1;
    private String adresse2;
    private int codePostal;
    private String ville;
    private String telephone;
    private String email;



    public Client(int numero, String raisonSoc, String adresse1, String adresse2, int codePostal, String ville, String telephone, String email){
        this.numero = numero;
        this.raisonSoc = raisonSoc;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal  = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.email = email;

    }
    public Client(String raisonSoc, String adresse1, String adresse2, int codePostal, String ville, String telephone, String email){
        this.raisonSoc = raisonSoc;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal  = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.email = email;
    }

    public void setNumero(int newNum){
        this.numero = newNum;
    }
    public int getNumero(){
        return this.numero;
    }
    public String getRaisonSoc(){
        return this.raisonSoc;
    }
    public String getAdresse1(){
        return this.adresse1;
    }
    public String getAdresse2(){
        return this.adresse2;
    }
    public int getCodePostal(){
        return this.codePostal;
    }
    public String getVille(){
        return this.ville;
    }
    public String getTelephone(){
        return this.telephone;
    }
    public String getEmail(){
        return this.email;
    }
}
