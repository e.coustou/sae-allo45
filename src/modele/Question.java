package modele;

import java.util.ArrayList;
import java.util.List;

public class Question {
    private String titre;
    private int numero;
    private char typeRep;
    private int valeurMax;
    private List<String> propositions;

    public Question(String titre, int num, char typeRep, int vMax){
        this.titre = titre;
        this.numero = num;
        this.typeRep = typeRep;
        this.valeurMax = vMax;
        this.propositions = new ArrayList<>();
    }
    public Question(){
        this.titre = "";
        this.numero = -1;
        this.typeRep = 'l';
        this.valeurMax = 0;
        this.propositions = new ArrayList<>();
    }


    public String getTitre(){
        return this.titre;
    }
    public int getNumero(){
        return this.numero;
    }
    public char getTypeR(){
        return this.typeRep;
    }
    public int getMaxValeur(){
        return this.valeurMax;
    }
    public List<String> getPropositions(){
        return this.propositions;
    }

    public void setTitre(String newTitre){
        this.titre = newTitre;
    }
    public void setNumero(int newNum){
        this.numero = newNum;
    }
    public void setTypeR(char newTypeRep){
        this.typeRep = newTypeRep;
    }
    public void setMaxValeur(int newvMax){
        this.valeurMax = newvMax;
    }
    public void ajouteProposition(String newP){
        this.propositions.add(newP);
    }
    public void ajoutePropositionVide(){
        this.propositions.add("");
    }
}