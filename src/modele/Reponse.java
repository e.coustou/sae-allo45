package modele;

public class Reponse {
    private String reponse;
    private int idSondage;
    private int idQuestion;
    private int idCSonde;

    public Reponse(String reponse, int idSondage, int idQuestion, int idCSonde){
        this.reponse = reponse;
        this.idSondage = idSondage;
        this.idQuestion = idQuestion;
        this.idCSonde = idCSonde;
    }
    public String getReponse(){
        return this.reponse;
    }
    public int getIdSondage(){
        return this.idSondage;
    }
    public int getIdQuestion(){
        return this.idQuestion;
    }
    public int getIdCSonde(){
        return this.idCSonde;
    }

    public void setVal(String reponse){
        this.reponse = reponse;
    }
    public void setNum(int idSondage){
        this.idSondage = idSondage;
    }
    public void setRandom(int idQuestion){
        this.idQuestion = idQuestion;
    }
    public void setRep(int idCSonde){
        this.idCSonde = idCSonde;
    }
}   
