package modele;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RequeteBD {
    private ConnexionMySQL laConnexion;
	private Statement st;
    
    public RequeteBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }

    public Utilisateur obtenirUtilisateur(String login, String mdp) throws SQLException{
		PreparedStatement ps = this.laConnexion.prepareStatement(
            "Select idU, nomU, prenomU, nomR from ROLEUTIL natural join UTILISATEUR where login=? and motDePasse=?");
        ps.setString(1, login);
        ps.setString(2, mdp);
        ResultSet rs = ps.executeQuery();

        rs.next();
        int idU = rs.getInt(1);
        String nomU = rs.getString(2);
        String prenomU = rs.getString(3);
        String role = rs.getString(4);
        Utilisateur utilisateur = new Utilisateur(idU, nomU, prenomU, role);
        rs.close();
        return utilisateur;
    }

    public List<Client> obtenirClients() throws SQLException{
        this.st = this.laConnexion.createStatement();
		ResultSet rs = this.st.executeQuery("Select * from CLIENT");

        List<Client> clients = new ArrayList<>();
        while(rs.next()){
            int id = rs.getInt(1);
            String raisonSoc = rs.getString(2);
            String adresse1 = rs.getString(3);
            String adresse2 = rs.getString(4);
            int codePostal = rs.getInt(5);
            String ville = rs.getString(6);
            String telephone = rs.getString(7);
            String email = rs.getString(8);
            Client client = new Client(id, raisonSoc, adresse1, adresse2, codePostal, ville, telephone, email);
            clients.add(client);
        }
        rs.close();
        return clients;
    }

    public Client ajouterClient(Client client) throws SQLException{
        this.st = this.laConnexion.createStatement();
		ResultSet rs = this.st.executeQuery("Select IFNULL(max(numC), 0) from CLIENT");
		rs.next();
		int dernierNumC = rs.getInt(1);
		rs.close();

        client.setNumero(dernierNumC + 1);
		PreparedStatement ps = this.laConnexion.prepareStatement(
			"insert into CLIENT values(?, ?, ?, ?, ?, ?, ?, ?)");

		ps.setInt(1, client.getNumero());
		ps.setString(2, client.getRaisonSoc());
		ps.setString(3, client.getAdresse1());
		ps.setString(4, client.getAdresse2());
        ps.setInt(5, client.getCodePostal());
        ps.setString(6, client.getVille());
        ps.setString(7, client.getTelephone());
        ps.setString(8, client.getEmail());

		ps.executeUpdate();
        return client;
    }

    public List<Panel> obtenirPanels() throws SQLException{
        this.st = this.laConnexion.createStatement();
		ResultSet rsPan = this.st.executeQuery("Select * from PANEL");

        List<Panel> panels = new ArrayList<>();
        while(rsPan.next()){
            int idPan = rsPan.getInt(1);
            String nomPan = rsPan.getString(2);
            Panel panel = new Panel(idPan, nomPan);
            PreparedStatement psSond = this.laConnexion.prepareStatement(
                "Select s.*, sexe from SONDE s natural join CARACTERISTIQUE natural join CONSTITUER where idPan=?");
            psSond.setInt(1, idPan);
            ResultSet rsSond = psSond.executeQuery();

            while(rsSond.next()){
                int idSond = rsSond.getInt(1);
                String nomSond = rsSond.getString(2);
                String prenomSond = rsSond.getString(3);
                String dateNaissSond = rsSond.getString(4);
                String telephoneSond = rsSond.getString(5);
                String idC = rsSond.getString(6);
                Character sexe = rsSond.getString(7).charAt(0);
                Sonde sonde = new Sonde(idSond, nomSond, prenomSond, dateNaissSond, telephoneSond, sexe, idC);
                panel.ajouteSonde(sonde);
            }
            rsSond.close();
            panels.add(panel);
        }
        rsPan.close();
        return panels;
    }

    public List<Sondage> obtenirSondages() throws SQLException{
        this.st = this.laConnexion.createStatement();
		ResultSet rsSonda = this.st.executeQuery("Select * from QUESTIONNAIRE");

        List<Sondage> sondages = new ArrayList<>();
        while(rsSonda.next()){
            int idSonda = rsSonda.getInt(1);
            String titreSonda = rsSonda.getString(2);
            Character etatSonda = rsSonda.getString(3).charAt(0);

            // pour obtenir le client
            int numC = rsSonda.getInt(4);
            PreparedStatement psClient = this.laConnexion.prepareStatement(
                "Select * from CLIENT where numC=?");
            psClient.setInt(1, numC);
            ResultSet rsClient = psClient.executeQuery();
            rsClient.next();
            String raisonSoc = rsClient.getString(2);
            String adresse1 = rsClient.getString(3);
            String adresse2 = rsClient.getString(4);
            int codePostal = rsClient.getInt(5);
            String ville = rsClient.getString(6);
            String telephone = rsClient.getString(7);
            String email = rsClient.getString(8);
            rsClient.close();
            Client clientSonda = new Client(numC, raisonSoc, adresse1, adresse2, codePostal, ville, telephone, email);

            // pour obtenir le panel et les sondés
            int idPan = rsSonda.getInt(6);
            PreparedStatement psPanel = this.laConnexion.prepareStatement(
                "Select * from PANEL where idPan=?");
            psPanel.setInt(1, idPan);
            ResultSet rsPanel = psPanel.executeQuery();
            rsPanel.next();
            String nomPan = rsPanel.getString(2);
            rsPanel.close();
            Panel panelSonda = new Panel(idPan, nomPan);

            PreparedStatement psSond = this.laConnexion.prepareStatement(
                "Select numSond, nomSond, prenomSond, dateNaisSond, telephoneSond, idC, sexe from SONDE natural join CARACTERISTIQUE natural join CONSTITUER where idPan=?");
            psSond.setInt(1, idPan);
            ResultSet rsSond = psSond.executeQuery();

            while(rsSond.next()){
                int idSond = rsSond.getInt(1);
                String nomSond = rsSond.getString(2);
                String prenomSond = rsSond.getString(3);
                String dateNaissSond = rsSond.getString(4);
                String telephoneSond = rsSond.getString(5);
                String idC = rsSond.getString(6);
                Character sexe = rsSond.getString(7).charAt(0);
                Sonde sonde = new Sonde(idSond, nomSond, prenomSond, dateNaissSond, telephoneSond, sexe, idC);
                panelSonda.ajouteSonde(sonde);
            }
            rsSond.close();

            // pour obtenir le concepteur
            int idU = rsSonda.getInt(5);
            PreparedStatement psUtilisateur = this.laConnexion.prepareStatement(
                "Select * from UTILISATEUR where idU=?");
            psUtilisateur.setInt(1, idU);
            ResultSet rsUtilisateur = psUtilisateur.executeQuery();
            rsUtilisateur.next();
            String nomU = rsUtilisateur.getString(2);
            String prenomU = rsUtilisateur.getString(3);
            int idRole = rsUtilisateur.getInt(6);
            rsUtilisateur.close();
            PreparedStatement psRole = this.laConnexion.prepareStatement(
                "select * from ROLEUTIL where idR=?");
            psRole.setInt(1, idRole);
            ResultSet rsRole = psRole.executeQuery();
            rsRole.next();
            String nomR = rsRole.getString(2);
            rsRole.close();
            Utilisateur conceptSonda = new Utilisateur(idU, nomU, prenomU, nomR);

            // cration du sondage dans le modèle
            Sondage sondage = new Sondage(idSonda, titreSonda, etatSonda, clientSonda, panelSonda, conceptSonda);


            // ajout des questions du sondage
            PreparedStatement psQuest = this.laConnexion.prepareStatement(
                "Select numQ, texteQ, MaxVal, idT from QUESTION natural join TYPEQUESTION where idQ=? order by numQ");
            psQuest.setInt(1, idSonda);
            ResultSet rsQuest = psQuest.executeQuery();
            while(rsQuest.next()){
                int numQ = rsQuest.getInt(1);
                String texte = rsQuest.getString(2);
                int MaxVal = rsQuest.getInt(3);
                Character idT = rsQuest.getString(4).charAt(0);
                Question question = new Question(texte, numQ, idT, MaxVal);

                PreparedStatement psValPoss = this.laConnexion.prepareStatement(
                    "Select idV, Valeur from VALPOSSIBLE where idQ=? and numQ=? order by idV");
                psValPoss.setInt(1, idSonda);
                psValPoss.setInt(2, numQ);
                ResultSet rsValPoss = psValPoss.executeQuery();
                while(rsValPoss.next()){
                    String valeur = rsValPoss.getString(2);
                    question.ajouteProposition(valeur);
                }
                sondage.ajouterQuestion(question);
            }
            rsQuest.close();
            sondages.add(sondage);
        }
        rsSonda.close();
        return sondages;
    }

    public void ajouterSondage(Sondage sondage) throws SQLException{
        try{supprimerSondage(sondage);}
        catch(SQLException e){System.out.println("Impossible de supprimer le sondage");}

        PreparedStatement psSonda = this.laConnexion.prepareStatement(
            "Insert into QUESTIONNAIRE(idQ, Titre, Etat, numC, idU, idPan) values(?, ?, ?, ?, ?, ?)");
        int idSondage = -1;
        if(sondage.getNumero() == -1){
            ResultSet rsIdSondage = this.st.executeQuery("Select IFNULL(max(idQ), 0) from QUESTIONNAIRE");
            rsIdSondage.next();
            idSondage = rsIdSondage.getInt(1)+1;
            psSonda.setInt(1, idSondage);
            rsIdSondage.close();
        }
        else{
            idSondage = sondage.getNumero();
            psSonda.setInt(1, idSondage);
        }
        sondage.setNumero(idSondage);
        psSonda.setString(2, sondage.getTitre());
        psSonda.setString(3, sondage.getEtat()+"");
        psSonda.setInt(4, sondage.getClient().getNumero());
        psSonda.setInt(5, sondage.getConcepteur().getId());
        psSonda.setInt(6, sondage.getPanel().getId());
        psSonda.executeUpdate();

        for(Question question: sondage.getQuestions()){
            PreparedStatement psQuest = this.laConnexion.prepareStatement(
                "Insert into QUESTION(idQ,numQ,texteQ,MaxVal,idT) values(?, ?, ?, ?, ?)");
            question.setNumero(sondage.getQuestions().indexOf(question));
            psQuest.setInt(1, sondage.getNumero());
            psQuest.setInt(2, question.getNumero());
            psQuest.setString(3, question.getTitre());
            psQuest.setInt(4, question.getMaxValeur());
            psQuest.setString(5, question.getTypeR()+"");
            psQuest.executeUpdate();
            for(int i=1; i<question.getPropositions().size(); i++){
                PreparedStatement psPropo = this.laConnexion.prepareStatement(
                    "Insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values(?, ?, ?, ?)");
                psPropo.setInt(1, sondage.getNumero());
                psPropo.setInt(2, question.getNumero());
                psPropo.setInt(3, i);
                psPropo.setString(4, question.getPropositions().get(i-1));
                psPropo.executeUpdate();
            }
        }
    }

    public void supprimerQuestion(Sondage sondage, Question question) throws SQLException{
        PreparedStatement psSupValPoss = this.laConnexion.prepareStatement(
            "Delete from VALPOSSIBLE where idQ=? and numQ=?");
        psSupValPoss.setInt(1, sondage.getNumero());
        psSupValPoss.setInt(2, question.getNumero());
        psSupValPoss.executeUpdate();

        PreparedStatement psSupQuest = this.laConnexion.prepareStatement(
            "Delete from QUESTION where idQ=? and numQ=?");
        psSupQuest.setInt(1, sondage.getNumero());
        psSupQuest.setInt(2, question.getNumero());
        psSupQuest.executeUpdate();
    }

    public void supprimerSondage(Sondage sondage) throws SQLException{
        for(Question question: sondage.getQuestions()){
            try{supprimerQuestion(sondage, question);}
            catch(SQLException e){System.out.println("Impossible de supprimer la question");}
        }
        PreparedStatement psSupSondage = this.laConnexion.prepareStatement(
            "Delete from QUESTIONNAIRE where idQ=?");
        psSupSondage.setInt(1, sondage.getNumero());
        psSupSondage.executeUpdate();
    }

    public boolean estInterroge(Sonde sonde, Sondage sondage) throws SQLException{
        PreparedStatement psEstInterroge = this.laConnexion.prepareStatement(
            "Select * from INTERROGER where numSond=? and idQ=?");
        psEstInterroge.setInt(1, sonde.getNumero());
        psEstInterroge.setInt(2, sondage.getNumero());
        ResultSet stEstInterroge = psEstInterroge.executeQuery();
        return stEstInterroge.next();
    }

    public void interroge(Utilisateur sondeur, Sonde sonde, Sondage sondage) throws SQLException{
        PreparedStatement psInterroger = this.laConnexion.prepareStatement(
            "Insert into INTERROGER(idU, numSond, idQ) values(?, ?, ?)");
        psInterroger.setInt(1, sondeur.getId());
        psInterroger.setInt(2, sonde.getNumero());
        psInterroger.setInt(3, sondage.getNumero());
        psInterroger.executeUpdate();
    }

    public void enregistrerReponse(Reponse reponse) throws SQLException{
        int idS = reponse.getIdSondage();
        int idQ = reponse.getIdQuestion();
        int idC = reponse.getIdCSonde();
        String valeur = reponse.getReponse();

        PreparedStatement psAjoutReponse = this.laConnexion.prepareStatement(
            "Insert into REPONDRE (idQ, numQ, idC, valeur) values(?, ?, ?, ?");
        psAjoutReponse.setInt(1, idS);
        psAjoutReponse.setInt(2, idQ);
        psAjoutReponse.setInt(3, idC);
        psAjoutReponse.setString(4, valeur);
    }
}
