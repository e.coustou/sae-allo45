package modele;

public class Utilisateur {

    /*
     * l'identifiant de l'utilisateur
     */
    private int id;
    /*
     * le nom de l'utilisateur
     */
    private String nom;
    /*
     * le prenom de l'utilisateur
     */
    private String prenom;
    /*
     * le role de l'utilisateur
     */
    private String role;

    public Utilisateur(int id, String nom, String prenom, String role){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
    }

    /**
     * retourne l'identifiiant de l'utilisateur
     * @return l'identifiant
     */
    public int getId(){
        return this.id;
    }

    /***
     * retourne le nom de l'utilisateur
     * @return le nom
     */
    public String getNom(){
        return this.nom;
    }

    /***
     * retourne le prenom de l'utilisateur
     * @return le prenom
     */
    public String getPrenom(){
        return this.prenom;
    }

    /***
     * retourne le role de l'utilisateur
     * @return le role
     */
    public String getRole(){
        return this.role;
    }
}