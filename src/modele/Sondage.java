package modele;
import java.util.ArrayList;
import java.util.List;

public class Sondage {
    private int numero;
    private String titre;
    private Character etat;
    private Client client;
    private Panel panel;
    private Utilisateur concepteur;
    private Question questionActuelle;
    private List<Question> listeQuestions;

    public Sondage(int numero, String titre, Character etat, Client client, Panel panel, Utilisateur concepteur){
        this.numero = numero; // nombre de sondage dans base de données + 1
        this.titre = titre;
        this.etat = etat;
        this.client = client;
        this.panel = panel;
        this.concepteur = concepteur;
        this.questionActuelle = null;
        this.listeQuestions = new ArrayList<>();
    }

    public Sondage(Utilisateur concepteur){
        this.numero = -1;
        this.titre = "";
        this.etat = 'C';
        this.client = null;
        this.panel = null;
        this.concepteur = concepteur;
        this.questionActuelle = null;
        this.listeQuestions = new ArrayList<>();
    }

    /**
     * change le numero du Sondage
     * @param numero le numero du sondage
     */
    public void setNumero(int numero){
        this.numero = numero;
    }
    /**
     * retourne le numero du sondage
     * @return le numero
     */
    public int getNumero(){
        return this.numero;
    }

    /**
     * Change le titre du sondage
     * @param titre le titre que porte le sondage
     */
    public void setTitre(String titre){
        this.titre = titre;
    }
    /**
     * Donne le titre du sondage
     * @return le titre que porte le sondage
     */
    public String getTitre(){
        return this.titre;
    }

    /**
     * Change le etat du sondage
     * @param etat le etat que porte le sondage
     */
    public void setEtat(Character etat){
        this.etat = etat;
    }
    /**
     * Donne le etat du sondage
     * @return le etat que porte le sondage
     */
    public Character getEtat(){
        return this.etat;
    }

    /**
     * Associe l'id de la société cliente qui a demander le sondage
     * @param client l'id de la société cliente
     */
    public void setClient(Client client){
        this.client = client;
    }
    /**
     * retourne le client 
     * @return le client du sondage
     */
    public Client getClient(){
        return this.client;
    }

    /**
     * Associe le panel qui sera intérroger pour le sondage
     * @param idPanel l'id du panel
     */
    public void setPanel(Panel panel){
        this.panel = panel;
    }
    /**
     * retourne le panel du sondage
     * @return le panel
     */
    public Panel getPanel(){
        return this.panel;
    }

    /**
     * retourne le concepteur du sondage
     * @return le concepteur
     */
    public Utilisateur getConcepteur(){
        return this.concepteur;
    }

    /**
     * Associe la question sur laquelle le concepteur travaille
     * @param question la question sur laquelle le concepteur travaille
     */
    public void setQuestionActuelle(Question question){
        this.questionActuelle = question;
    }
    /**
     * retourne la question sur laquelle le concepteur travaille
     * @return la question sur laquelle le concepteur travaille
     */
    public Question getQuestionActuelle(){
        return this.questionActuelle;
    }

    /**
     * retourne la liste des question du sondage
     * @return la liste des sondage
     */
    public List<Question> getQuestions(){
        return this.listeQuestions;
    }
    /**
     * ajoute une question compète dans la liste des questions
     * @param question la question
     * @throws QuestionDejaPresenteException
     */
    public void ajouterQuestion(Question question){
        this.listeQuestions.add(question);
    }
    /**
     * ajoute une nouvelle question dans la liste des questions
     * et qui devient la question actuelle
     */
    public void ajouterNouvelleQuestion(){
        Question question = new Question();
        this.questionActuelle = question;
    }

    public void supprimerQuestion(Question question){
        int i = this.listeQuestions.indexOf(question);
        this.listeQuestions.remove(i);
    }

    public void questionPrecedente(){
        int numeroQuestionActuelle = this.questionActuelle.getNumero();
        for(Question question : this.listeQuestions){
            if(question.getNumero()+1 == numeroQuestionActuelle){
                this.questionActuelle = question;
                break;
            }
        }
    }
    public void questionSuivante(){
        int numeroQuestionActuelle = this.questionActuelle.getNumero();
        for(Question question : this.listeQuestions){
            if(question.getNumero()-1 == numeroQuestionActuelle){
                this.questionActuelle = question;
                break;
            }
        }
    }
}
