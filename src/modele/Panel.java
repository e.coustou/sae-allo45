package modele;
import java.util.ArrayList;
import java.util.List;

public class Panel{
    private int id;
    private String nom;
    private List<Sonde> listeSondes;
    private Sonde sondeActuel;

    public Panel(int id, String nom){
        this.id = id;
        this.nom = nom;
        this.listeSondes = new ArrayList<>();

    }

    public Sonde getSondeActuel() {
        return sondeActuel;
    }
    public void setSondeActuel(Sonde sondeActuelle) {
        this.sondeActuel = sondeActuelle;
    }


    public int getId(){
        return this.id;
    }
    public String getNom(){
        return this.nom;
    }
    public void ajouteSonde(Sonde sonde){
        this.listeSondes.add(sonde);
    }
    public List<Sonde> getSondes(){
        return this.listeSondes;
    }
}