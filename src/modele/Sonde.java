package modele;
public class Sonde{
    private int numero;
    private String nom;
    private String prenom;
    private String dateNaiss;
    private String telephone;
    private char sex;
    private String idC;

    public Sonde(int numero, String nom, String prenom, String dateNaiss, String telephone,  char sex, String idC){
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaiss = dateNaiss;
        this.telephone = telephone;
        this.sex = sex;
        this.idC = idC;
    }

    public int getNumero(){
        return this.numero;
    }
    public String getNom(){
        return this.nom;
    }
    public String getPrenom(){
        return this.prenom;
    }
    public String getDateNaiss(){
        return this.dateNaiss;
    }
    public String getTelephone(){
        return this.telephone;
    }
    public char getSex() {
        return this.sex;
    }    
    public String getIdC(){
        return this.idC;
    }
}
