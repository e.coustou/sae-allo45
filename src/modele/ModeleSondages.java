package modele;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ModeleSondages{
    /**
     * La liste de tous les sondages qui ont été crées
     */
    private List<Sondage> listeSondages;
    /**
     * Le sondage qui est en train d'être modifier (on lui ajoute/enlève des questions)
     */
    private Sondage sondageActuel;
    /**
     * la classe qui contient les méthodes pour la base de donnée
     */
    private RequeteBD requeteBD;



    public ModeleSondages(RequeteBD requeteBD){
        this.requeteBD = requeteBD;
        try{
            this.listeSondages = new ArrayList<>(this.requeteBD.obtenirSondages());
        }catch(SQLException e){
            System.out.println("Imposible d'obtenir les sondages");
        }
        this.sondageActuel = listeSondages.get(0);
        // initialise avec tous les sondages qui sont présent dans la BD
    }

    /**
     * retourne la liste des sondages
     * @return la liste des sondages
     */
    public List<Sondage> getSondages(){
        return this.listeSondages;
    }
    /**
     * retourne la classe RequeteBD avec toutes les requetes pour la base de donnéess
     * @return la classe RequeteBD
     */
    public RequeteBD getRequetes(){
        return this.requeteBD;
    }
    /**
     * retourne le sondage dans lequel le concepteur est en train d'ajouter ou enlever des questions
     * @return Le sondage dans lequel le concepteur est en train d'ajouter ou enlever des questions
     */
    public Sondage getSondageActuel(){
        return this.sondageActuel;
    }
    
    /**
     * Associe le sondage dans lequel le concepteur ajoute ou enlève des questions
     * @param sondage sondage qui est en train d'être modifié
     */
    public void setSondageActuel(Sondage sondage){
        this.sondageActuel = sondage;
    }

    /**
     * ajoute un sondage à la liste des sondages
     */
    public void ajouterSondage(Utilisateur concepteur){
        Sondage sondage = new Sondage(concepteur);
        this.listeSondages.add(sondage);
        this.sondageActuel = sondage;
    }
    /**
     * supprime le sondage de la liste des sondages
     * @param sondage
     */
    public void supprimerSondage(Sondage sondage) {
        int i = this.listeSondages.indexOf(sondage);
        this.listeSondages.remove(i);
        try{
            this.requeteBD.supprimerSondage(sondage);
        }catch(SQLException e){
            System.out.println("Impossible de supprimer le sondage");
        }
    }
    /**
     * permet d'enregistrer le Sondage actuel dans la base de données
     * @param titre
     */
    public void enregistrerSondage(String titre) {
        getSondageActuel().setTitre(titre);
        try{
            requeteBD.ajouterSondage(getSondageActuel());
        }
        catch(SQLException e){
            System.out.println("enregistrement impossible");
        }
    }
    /**
     * permet de valider le sondage pour qu'il soit accessible dans le sondeur
     * @param sondage le sondage prêt pour le sondeur
     */
    public void validerSondage(Sondage sondage){
        sondage.setEtat('S');
        try{
            this.requeteBD.ajouterSondage(sondage);
        }catch(SQLException e){
            System.out.println("Impossible de valider le sondage");
        }
    }

    public void supprimerQuestion(Sondage sondage, Question question){
        sondage.supprimerQuestion(question);
        try{
            this.requeteBD.supprimerQuestion(sondage, question);
        }catch(SQLException e){
            System.out.println("Impossible de supprimer le sondage");
        }
    }
    
    /**
     * initialise pour le sondeur, le sondé qu'il va appeler et le sondage sur lequel il va l'intérroger  
     * @throws SQLException
     */
    public void initSetUpSondeur(Utilisateur sondeur){
        try {
            for(Sondage sondage : this.listeSondages){
                if(sondage.getEtat()!='C' && sondage.getEtat()!='A'){
                    this.sondageActuel = sondage;
                    for(Sonde sonde : this.sondageActuel.getPanel().getSondes()){
                        if(!(requeteBD.estInterroge(sonde, this.sondageActuel))){
                            this.sondageActuel.getPanel().setSondeActuel(sonde);
                            interrogerSonde(sondeur);
                            break;
                        }
                    }
                }
            }
            Question q = this.sondageActuel.getQuestions().get(0);
            this.sondageActuel.setQuestionActuelle(q);
        }
        catch (SQLException e) {
            System.out.println("Impossible d'initialiser les éléments pour le sondeur");
        }
    }

    private void interrogerSonde(Utilisateur sondeur){
        Sonde sonde = this.sondageActuel.getPanel().getSondeActuel();
        try {
            this.requeteBD.interroge(sondeur, sonde, this.sondageActuel);            
        } catch (SQLException e) {
            System.out.println("Impossible d'interroger un sondé");
        }
    }

    public void ajoutProposition(){
        this.sondageActuel.getQuestionActuelle().ajoutePropositionVide();
    }
}
