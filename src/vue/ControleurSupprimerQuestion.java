package vue;
import modele.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurSupprimerQuestion implements EventHandler<ActionEvent> {
    private ModeleSondages modele;
    private Sondage sondage;
    private Question question;
    private FenetreConcepteur vue;

    public ControleurSupprimerQuestion(ModeleSondages modele, Sondage sondage, Question question, FenetreConcepteur vue){
        this.modele = modele;
        this.sondage = sondage;
        this.question = question;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent event) {
        this.modele.supprimerQuestion(this.sondage, this.question);
        this.vue.afficheSondage();
    }
}
