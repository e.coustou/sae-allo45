package vue;
import modele.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerQuestion implements EventHandler<ActionEvent>{
    private Sondage sondage;
    private FenetreConcepteur vue;

    public ControleurCreerQuestion(Sondage sondage, FenetreConcepteur vue) {
        this.sondage = sondage;
        this.vue = vue;
    }
    @Override
    public void handle(ActionEvent event){
        this.sondage.ajouterNouvelleQuestion();
        this.vue.afficheQuestion();
    }
}
