
package vue;
import modele.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerSondage implements EventHandler<ActionEvent>{
    private ModeleSondages modele;
    private Utilisateur concepteur;
    private FenetreConcepteur vue;

    public ControleurCreerSondage(ModeleSondages modele, Utilisateur concepteur, FenetreConcepteur vue){
        this.modele = modele;
        this.concepteur = concepteur;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent event){
        this.modele.ajouterSondage(this.concepteur);
        this.vue.afficheSondage();
    }

}