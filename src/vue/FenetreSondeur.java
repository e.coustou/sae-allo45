package vue;
import modele.*;

import java.util.List;

import javax.swing.border.Border;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class FenetreSondeur extends BorderPane {
        private Allo45 appli;
        private HBox top;
        private BorderPane center;
        private Utilisateur user;
        private Button boutonDeconnexion;
        private ModeleSondages ListS;

    public FenetreSondeur(Allo45 appli,Button boutonconnect, Utilisateur user, ModeleSondages ListS){
        this.appli = appli;
        this.boutonDeconnexion = boutonconnect;
        this.user = user;
        this.ListS = ListS;
        this.top = top();
        this.center = center();
        this.setTop(this.top);
        this.setCenter(this.center);
        
    }

    private HBox top(){
        
        HBox top = new HBox() {{
            setStyle("-fx-background-color: #A9A9A9;");
        }};

        GridPane infosUtilisateur = new GridPane();
        Label nom = new Label(this.user.getNom());
        Label prenom = new Label(this.user.getPrenom());
        infosUtilisateur.add(nom, 0, 0);
        infosUtilisateur.add(prenom, 0, 1);
        infosUtilisateur.add(this.boutonDeconnexion, 0, 2);
        infosUtilisateur.setStyle("-fx-background-color: #5C93E7; -fx-vgap: 10; -fx-padding: 10px;");
        GridPane.setHalignment(nom, HPos.CENTER);
        GridPane.setHalignment(prenom, HPos.CENTER);
        Label labelNomModule = new Label("         Module Sondeur  ");
        labelNomModule.setStyle("-fx-font-size: 35px; -fx-font-weight: bold; -fx-padding: 30px;");

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        //Dans la hBox
        Button buttonhBox = new Button("Sondé Suivant");
        buttonhBox.setStyle("-fx-font-size: 20px;");
        buttonhBox.setOnAction(new ControleurSondeSuivant(this.ListS, this, this.user)); 
        HBox.setMargin(buttonhBox, new Insets(20));      
        hBox.getChildren().addAll(labelNomModule, buttonhBox);

        VBox vBox2 = new VBox();
        //Dans le Hbox
        Label label = new Label("Nom, Prenom sondé : " + ListS.getSondageActuel().getPanel().getSondeActuel().getNom()+","+ListS.getSondageActuel().getPanel().getSondeActuel().getPrenom());
        Label label2 = new Label("Date de naissance : " + ListS.getSondageActuel().getPanel().getSondeActuel().getDateNaiss());
        Label label3 = new Label("Nom questionnaire : " + ListS.getSondageActuel().getTitre());
        Label label4 = new Label("Date : " + java.time.LocalDate.now());
        Label label5 = new Label("Sexe : " + ListS.getSondageActuel().getPanel().getSondeActuel().getSex());
        vBox2.getChildren().addAll(label,label2,label3,label4,label5);
        vBox2.setStyle("-fx-background-color: #5C93E7; -fx-padding:20;");


        top.getChildren().addAll(infosUtilisateur,hBox,vBox2);
        
        return top;
    }

    private BorderPane center(){

        BorderPane center = new BorderPane();
        

        HBox hBox = new HBox() {{
            setStyle("-fx-border-color: #000000; -fx-border-radius: 10; -fx-border-width: 3px;-fx-font-size: 14px;");
        }};
        hBox.setAlignment(Pos.CENTER);
        //dans le hbox
        Button bouttonQuestionP = new Button("Question précédente");
        bouttonQuestionP.setOnAction(new ControleurQuestionPrecedente(this.ListS.getSondageActuel(), this));
        Text question = new Text(this.ListS.getSondageActuel().getQuestionActuelle().getTitre());
        Text numQuestion = new Text(String.valueOf(this.ListS.getSondageActuel().getQuestionActuelle().getNumero()+" / "+this.ListS.getSondageActuel().getQuestions().size()));
        Button bouttonQuestionS = new Button("Question Suivante");
        bouttonQuestionS.setOnAction(new ControleurQuestionSuivante(this.ListS.getSondageActuel(), this));
        HBox.setMargin(bouttonQuestionP, new Insets(20));
        HBox.setMargin(bouttonQuestionS, new Insets(20));
        hBox.getChildren().addAll(bouttonQuestionP,question,numQuestion,bouttonQuestionS);        
        hBox.setSpacing(20);
        center.setTop(hBox);

       switch(String.valueOf(this.ListS.getSondageActuel().getQuestionActuelle().getTypeR())){
            //  switch("m"){
            case "l" :
                //Choix libre
                TextArea choixL = new TextArea();
                choixL.setPromptText("Votre réponse : ");
                choixL.setMaxSize(800, 250);
                center.setCenter(choixL);
                break;
            case "m" :  
                //Choix multiple 
                TilePane choixM = new TilePane();
                for(int i = 0; i < this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().size(); i++){
                    CheckBox c = new CheckBox(this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().get(i));
                    choixM.setMargin(c, new Insets(20));
                    choixM.getChildren().add(c);
                }
                choixM.setMaxSize(800, 150);
                center.setCenter(choixM);
                break;
            case "u" :
                //Choix simple
                ToggleGroup groupB = new ToggleGroup();
                TilePane choixS = new TilePane();
                for(int i = 0; i < this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().size(); i++){
                    RadioButton r = new RadioButton(this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().get(i));
                    r.setToggleGroup(groupB);
                    choixS.setMargin(r, new Insets(20));
                    choixS.getChildren().add(r);
                }
                choixS.setMaxSize(800, 150);
                center.setCenter(choixS);
                break;
            case "c" :
                //classement
                TilePane classement = new TilePane();
                for(int i=0; i< this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().size(); i++){
                    HBox cla = new HBox();
                    Label l = new Label(this.ListS.getSondageActuel().getQuestionActuelle().getPropositions().get(i));
                    TextField t = new TextField();
                    cla.getChildren().addAll(l,t);
                    cla.setMargin(l, new Insets(5));
                    classement.getChildren().add(cla);
                    classement.setMargin(cla, new Insets(20));
                }
                classement.setMaxSize(825, 200);
                center.setCenter(classement);

                break;
            default :
                //Note
                ComboBox<Integer> note = new ComboBox<Integer>();
                for(int i=0; i< this.ListS.getSondageActuel().getQuestionActuelle().getMaxValeur() + 1 ; i++){
                    note.getItems().add(i);
                }
                note.setStyle("-fx-font-size : 25;");
                note.setMaxSize(350, 100);
                note.setPromptText("Attribuez votre note");
                note.setOnAction(new ControleurEnregistrerReponseSondeur(this, ListS));
                center.setCenter(note);
            }
        center.setPadding(new Insets(10));
        if(this.ListS.getSondageActuel().getQuestionActuelle().getNumero() == this.ListS.getSondageActuel().getQuestions().size()){
            bouttonQuestionS.setText("Terminer le questionnaire");
        }
        return center;
    }
    public void majafficheFenetreSondeur(){this.top = top();this.center = center();this.setTop(this.top);this.setCenter(this.center);}
    
}

