package vue;
import modele.*;

import java.sql.SQLException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class Allo45 extends Application{
    private ConnexionMySQL connexionMySQL;
    private Button boutonDeconnexion;
    private Stage stage;
    private Scene scene;
    private ModeleSondages listeSondages;

    @Override
    public void init() {
        try {
            this.connexionMySQL = new ConnexionMySQL();
            this.connexionMySQL.connecter("127.0.0.1", "sondage", "root", "mdp_root");
            System.out.println("Connexion");
            RequeteBD requetes = new RequeteBD(this.connexionMySQL);
            this.listeSondages = new ModeleSondages(requetes);
        }catch (ClassNotFoundException ex){
            System.out.println("Driver MySQL non trouvé!!!");
            System.exit(1);
        }
        catch(SQLException e){
            System.out.println("Connexion impossible");
        }
        this.boutonDeconnexion = new Button("Déconnexion");
        this.boutonDeconnexion.setOnAction(new ControleurDeconnexion(this));
    }
    
    @Override
    public void start(Stage stage) {
        this.stage = stage;
        this.stage.setTitle("Allo45");
        this.scene = new Scene(new FenetreConnexion(this, this.listeSondages));
        this.stage.setScene(this.scene);
        this.stage.show();
    }

    public ConnexionMySQL getConnexionSQL() {
        return this.connexionMySQL;
    }

    public void afficheFenetreConnexion() {
        BorderPane root = new FenetreConnexion(this, this.listeSondages);
        this.scene = new Scene(root);
        this.stage.setScene(this.scene);
        this.stage.sizeToScene();
        this.stage.show();
    }

    public void afficheFenetreSondeur(Utilisateur user) {
        BorderPane root = new FenetreSondeur(this, this.boutonDeconnexion, user, this.listeSondages);
        this.scene = new Scene(root);
        this.stage.setScene(this.scene);
        this.stage.setWidth(1200);
        this.stage.show();
    }
    
    public void afficheFenetreConcepteur(Utilisateur user) {
        BorderPane root = new FenetreConcepteur(this, this.boutonDeconnexion, user, this.listeSondages);
        this.scene = new Scene(root);
        this.stage.setScene(this.scene);
        this.stage.sizeToScene();
        this.stage.show();
    }

    public Alert popupDeconnexion(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Voulez vous vraiment vous déconnecter ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public void quitte(){
        Platform.exit();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

