package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.Question;
import modele.Sondage;

public class ControleurModifierQuestion implements EventHandler<ActionEvent> {
    /**
     * C'est la question que l'on veut modifier
     */
    private Question question;
    /**
     * C'est le Sondage de la question
     */
    private Sondage sondage;
    /**
     * C'est la vue ou sont les méthodes d'affichage
     */
    private FenetreConcepteur appli;


    public ControleurModifierQuestion(FenetreConcepteur appli, Question question, Sondage sondage){
        this.appli = appli;
        this.question = question;
        this.sondage = sondage;
    }

    @Override
    public void handle(ActionEvent event) {
        this.sondage.setQuestionActuelle(question);
        this.appli.afficheQuestion();
    }
}
