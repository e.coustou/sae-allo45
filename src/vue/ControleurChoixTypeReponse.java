package vue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import modele.ModeleSondages;

public class ControleurChoixTypeReponse implements EventHandler<ActionEvent> {
    private Allo45 allo45;
    private FenetreConcepteur fenetreConcepteur;
    private ModeleSondages modele;

    public ControleurChoixTypeReponse(Allo45 allo45, FenetreConcepteur fenetreConcepteur, ModeleSondages modele) {
        this.allo45 = allo45;
        this.fenetreConcepteur = fenetreConcepteur;
        this.modele = modele;
    }

    @Override
    public void handle(ActionEvent event) {
        ComboBox<String> reponses = (ComboBox<String>) event.getSource();
        String rep = reponses.getValue();
        switch (rep) {
            case "Classement": this.modele.getSondageActuel().getQuestionActuelle().setTypeR('c'); break;
            case "Choix unique": this.modele.getSondageActuel().getQuestionActuelle().setTypeR('u'); break;
            case "Choix multiple": this.modele.getSondageActuel().getQuestionActuelle().setTypeR('m'); break;
            case "Note": this.modele.getSondageActuel().getQuestionActuelle().setTypeR('n'); break;
            default: this.modele.getSondageActuel().getQuestionActuelle().setTypeR('l');
        }
        this.fenetreConcepteur.afficheQuestion();
    }
}
