package vue;
import modele.*;
import exceptions.*;

import java.sql.SQLException;
import java.util.List;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class FenetreConcepteur extends BorderPane {
    private Allo45 allo45;
    private Button boutonDeconnexion;
    private ModeleSondages modeleSondages;
    private Utilisateur user;

    private TextField tfTitreSondage;
    private ComboBox<String> cbClient;
    private ComboBox<String> cbPanel;
    // private List<Question> questionsSondage;

    //fenetre conception question
    private TextField tffTitreQuestion;
    private TextField tfValMaxQuestion;
    private ComboBox<String> typeReponse;


    public FenetreConcepteur(Allo45 allo45, Button boutonDeconnexion, Utilisateur user, ModeleSondages modeleSondages) {
        this.allo45 = allo45;
        this.boutonDeconnexion = boutonDeconnexion;
        this.user = user;
        this.modeleSondages = modeleSondages;
        this.tffTitreQuestion = new TextField();
        this.typeReponse = new ComboBox<String>();
        this.typeReponse.setOnAction(new ControleurChoixTypeReponse(this.allo45, this, this.modeleSondages));
        this.typeReponse.getItems().addAll("Libre", "Choix unique", "Choix multiple", "Note", "Classement");
        this.typeReponse.setValue("Libre");

        this.setTop(this.ajouteTop());
        this.setCenter(this.fenetreAccueil());
    }


    private VBox fenetreAccueil() {
        VBox center = new VBox() {{
            setStyle("-fx-alignment: center; -fx-padding: 50px; -fx-hgap: 40; -fx-vgap: 40; -fx-spacing: 50;"+
            "-fx-border-color: #000000; -fx-border-insets: 50 30 30 30; -fx-border-radius: 10px; -fx-border-width: 3px;"+
            "-fx-background-color: #AAAAAA; -fx-background-radius: 10px; -fx-background-insets: 50 30 30 30;");
        }};

        Text listeSond = new Text("Liste des sondages") {{
            setStyle("-fx-font-size: 20;");
        }};

        VBox sondages = new VBox() {{
            setStyle("-fx-background-color: #AAAAAA; -fx-spacing: 20; -fx-padding: 30;");
        }};
        for (int i=0; i<this.modeleSondages.getSondages().size(); i++) {
            Sondage s = this.modeleSondages.getSondages().get(i);
            if (s.getEtat().equals('C')) {
                HBox sond = new HBox() {{
                    setStyle("-fx-spacing: 10;");
                }};
                String nomSondage = s.getTitre();
                Label nomSond = new Label(nomSondage);
                Button modifier = new Button("Modifier");
                Button supprimer = new Button("❌") {{
                    setStyle("-fx-color: #FF0000;");
                    setOnMouseEntered(e -> setStyle("-fx-color: #FF4000;"));
                    setOnMouseExited(e -> setStyle("-fx-color: #FF0000;"));
                }};
                Button valider = new Button("✓") {{
                    setStyle("-fx-color: #00C800;");
                    setOnMouseEntered(e -> setStyle("-fx-color: #00D400;"));
                    setOnMouseExited(e -> setStyle("-fx-color: #00C800;"));
                }};
                modifier.setOnAction(new ControleurModifierSondage(this, s, this.modeleSondages));
                supprimer.setOnAction(new ControleurSuppSondage(this.modeleSondages, s, this));
                valider.setOnAction(new ControleurValiderSondage(this.modeleSondages, this, s));
                sond.getChildren().addAll(valider, modifier, supprimer, nomSond);
                sondages.getChildren().add(sond);
            }
        }
        if (sondages.getChildren().size() == 0) {
            sondages.getChildren().add(new Label("Aucun sondage en construction"){{setStyle("-fx-font-size: 20;");}});
        }
        
        Button boutonCreerSondage = new Button("Créer un sondage");
        boutonCreerSondage.setOnAction(new ControleurCreerSondage(this.modeleSondages, this.user, this));

        center.getChildren().addAll(listeSond, sondages, boutonCreerSondage);

        return center;
    }

    public VBox fenetreSondage(){

        Sondage s = this.modeleSondages.getSondageActuel();

        VBox vBox = new VBox() {{
            setStyle("-fx-alignment: center; -fx-spacing: 10; -fx-margin: 20;");
        }};

        Label titre = new Label("Titre du sondage : ");
        this.tfTitreSondage = new TextField();

        Label client = new Label("Nom Entreprise : ");
        this.cbClient = new ComboBox<>();

        Label panel = new Label("Le panel :");
        this.cbPanel = new ComboBox<>();

        Label listeQuestions = new Label("Les questions :");
        
        VBox questions = new VBox() {{
            setStyle("-fx-spacing: 10; -fx-background-color: #A9A9A9; -fx-padding: 10;");
        }};

        for (int i=0; i<this.modeleSondages.getSondageActuel().getQuestions().size(); i++) {
            HBox quest = new HBox() {{
                setStyle("-fx-spacing: 10;");
            }};

            Question q = this.modeleSondages.getSondageActuel().getQuestions().get(i);
            String nomQuestion = q.getTitre();
            Label nomQuest = new Label(nomQuestion);
            Button modifier = new Button("Modifier");
            Button supprimer = new Button("❌") {{
                setStyle("-fx-color: #FF0000;");
                setOnMouseEntered(e -> setStyle("-fx-color: #FF4000;"));
                setOnMouseExited(e -> setStyle("-fx-color: #FF0000;"));
            }};
            modifier.setOnAction(new ControleurModifierQuestion(this, q, this.modeleSondages.getSondageActuel()));
            supprimer.setOnAction(new ControleurSupprimerQuestion(this.modeleSondages, this.modeleSondages.getSondageActuel(), q, this));
            quest.getChildren().addAll(modifier, supprimer, nomQuest);
            questions.getChildren().add(quest);
        }

        if (s.getNumero() != -1) {
            this.tfTitreSondage.setText(s.getTitre());
            this.cbClient.setValue(s.getClient().getRaisonSoc());
            this.cbPanel.setValue(s.getPanel().getNom());
        }
        if (s.getQuestions().size() == 0) {
            questions.getChildren().add(new Label("Il n'y a pas de questions dans ce sondage"));
        }
        try {
            for (Client c: this.modeleSondages.getRequetes().obtenirClients()) {
                this.cbClient.getItems().add(c.getRaisonSoc());
            }
            for (Panel p: this.modeleSondages.getRequetes().obtenirPanels()) {
                this.cbPanel.getItems().add(p.getNom());
            }
        } catch (SQLException e) {}

        HBox hBox = new HBox();
        Button boutonCreerQuestion = new Button("Ajouter Question");
        boutonCreerQuestion.setOnAction(new ControleurCreerQuestion(this.modeleSondages.getSondageActuel(), this));
        Button boutonEnregisterSondage = new Button("Enregistrer Sondage");
        boutonEnregisterSondage.setOnAction(new ControleurEnregistrerSondage(this.modeleSondages, this));

        hBox.getChildren().addAll(boutonCreerQuestion, boutonEnregisterSondage);
        vBox.getChildren().addAll(titre, this.tfTitreSondage, client, this.cbClient, panel, this.cbPanel, listeQuestions, questions, hBox);

        return vBox;
    }

    private VBox fenetreCreationQuestion() {
        VBox center = new VBox(){{
            setSpacing(20);
        }};
        Label labTitreSondage = new Label(this.modeleSondages.getSondageActuel().getTitre());

        Label labquestion = new Label("Question");

        Label labChoixTypeQuestion = new Label("Choisir le type de la question");

        HBox boutonsBas = new HBox();
        Button retournerSondage = new Button("Retourner au sondage");
        retournerSondage.setOnAction(new ControleurRetournerSondage(this));
        Button enregistrerQuestion = new Button("Enregistrer question");
        enregistrerQuestion.setOnAction(new ControleurEnregistrerQuestion(this.modeleSondages.getSondageActuel(), this));
        boutonsBas.getChildren().addAll(retournerSondage, enregistrerQuestion);

        center.getChildren().addAll(labTitreSondage, labquestion, this.tffTitreQuestion, labChoixTypeQuestion, typeReponse, choixTypeQuestion(), boutonsBas); 
        return center;
    }

    private Pane choixTypeQuestion(){
        GridPane choixProposition = new GridPane();
        switch(this.modeleSondages.getSondageActuel().getQuestionActuelle().getTypeR()){
            case 'u': 
                return afficheCreationReponseSimpleClassement();
            case 'm':
                return afficheCreationReponseMultiple();
            case 'c':
                return afficheCreationReponseSimpleClassement();
            case 'n':
                return afficheCreationReponseNote();
            default :
                return choixProposition;
        }
    }

    private GridPane afficheCreationReponseSimpleClassement(){
        GridPane choixProposition = new GridPane();
        List<String> listeProposition = this.modeleSondages.getSondageActuel().getQuestionActuelle().getPropositions();
        int posNbRep = 0;
        for(int indice = 0; indice<listeProposition.size(); indice++){
            TextField textChoix = new TextField(listeProposition.get(indice));
            textChoix.setOnMouseClicked(new ControleurAjoutChoix(this.modeleSondages));
            choixProposition.add(textChoix, 0, indice);
            Button suppProposition = new Button("❌");
            suppProposition.setOnAction(new ControleurSupprimerChoix(this.modeleSondages, listeProposition.get(indice), this));
            choixProposition.add(suppProposition, 1, indice);
            posNbRep = indice;
        }
        Button ajoutChoix = new Button("+");
        ajoutChoix.setOnAction(new ControleurCreerChoix(this.modeleSondages, this));
        choixProposition.add(ajoutChoix, 0, posNbRep+1);

        return choixProposition;
    }

    private GridPane afficheCreationReponseMultiple(){
        GridPane choixProposition = new GridPane();
        List<String> listeProposition = this.modeleSondages.getSondageActuel().getQuestionActuelle().getPropositions();
        int posNbRep = 0;
        for(int indice = 0; indice<listeProposition.size(); indice++){
            choixProposition.add(new TextField(listeProposition.get(indice)), 0, indice);
            Button suppProposition = new Button("❌");
            suppProposition.setOnAction(new ControleurSupprimerChoix(this.modeleSondages, listeProposition.get(indice), this));
            choixProposition.add(suppProposition, 1, indice);
            posNbRep = indice;
        }
        TextField tfNbRepMax = new TextField();
        tfNbRepMax.setPromptText("Nombre de réponses maximum");
        choixProposition.add(tfNbRepMax, 0, posNbRep+1);

        Button ajoutChoix = new Button("+");
        ajoutChoix.setOnAction(new ControleurCreerChoix(this.modeleSondages, this));
        choixProposition.add(ajoutChoix, 0, posNbRep+2);

        return choixProposition;
    }

    private Pane afficheCreationReponseNote(){
        Pane boiteNotreMax = new Pane();
        this.tfValMaxQuestion = new TextField(this.modeleSondages.getSondageActuel().getQuestionActuelle().getMaxValeur()+"");
        this.tfValMaxQuestion.setPromptText("Note maximale");
        boiteNotreMax.getChildren().add(this.tfValMaxQuestion);
        return boiteNotreMax;
    }





    private HBox ajouteTop() {
        HBox top = new HBox();
        GridPane infosUtilisateur = new GridPane();
        Label nom = new Label(this.user.getNom());
        Label prenom = new Label(this.user.getPrenom());
        infosUtilisateur.add(nom, 0, 0);
        infosUtilisateur.add(prenom, 0, 1);
        infosUtilisateur.add(this.boutonDeconnexion, 0, 2);
        infosUtilisateur.setStyle("-fx-background-color: #5C93E7; -fx-vgap: 10; -fx-padding: 10px;");
        GridPane.setHalignment(nom, HPos.CENTER);
        GridPane.setHalignment(prenom, HPos.CENTER);

        Label labelNomModule = new Label("   Module Concepteur  ");
        labelNomModule.setStyle("-fx-font-size: 40px; -fx-font-weight: bold; -fx-padding: 30px;");

        top.setStyle("-fx-background-color: #A9A9A9");
        top.getChildren().add(infosUtilisateur);
        top.getChildren().add(labelNomModule);

        return top;
    }

    public void afficheAccueil() {this.setCenter(this.fenetreAccueil());}
    public void afficheSondage() {this.setCenter(this.fenetreSondage());}
    public void afficheQuestion() {this.setCenter(this.fenetreCreationQuestion());}



    //méthodes conception Sondage :
    public String getTitreS() throws PasDeTitreSondageException {
        if (this.tfTitreSondage.getText().equals("")) {
            throw new PasDeTitreSondageException();
        }
        return this.tfTitreSondage.getText();
    }

    public void setTitreS(Sondage sondage){
        String titre = sondage.getTitre();
        this.tfTitreSondage.setText(titre);
    }

    public Client getClientS() throws PasDeClientSondageException{
        if (this.cbClient.getValue() == null) {
            throw new PasDeClientSondageException();
        }
        try {
            for(Client client : this.modeleSondages.getRequetes().obtenirClients()){
                if(client.getRaisonSoc().equals(this.cbClient.getValue())){
                    return client;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void setClientS(Client client){
        String resSoc = client.getRaisonSoc();
        this.cbClient.setValue(resSoc);
    }

    public Panel getPanelS() throws PasDePanelSondageException{
        if (this.cbPanel.getValue() == null) {
            throw new PasDePanelSondageException();
        }
        try {
            for(Panel panel : this.modeleSondages.getRequetes().obtenirPanels()){
                if(panel.getNom().equals(this.cbPanel.getValue())){
                    return panel;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void setPanelS(Panel panel){
        String nomPan = panel.getNom();
        this.cbPanel.setValue(nomPan);
    }

    //méthodes conception question

    public String getTitreQ() throws PasDeTitreQuestionException{
        if (this.tffTitreQuestion.getText().equals("")) {
            throw new PasDeTitreQuestionException();
        }
        return this.tffTitreQuestion.getText();
    }

    public void setTitreQ(String titre){
        this.tffTitreQuestion.setText(titre);
    }

    public Character getTypeR() {
        return this.modeleSondages.getSondageActuel().getQuestionActuelle().getTypeR();
    }

    public int getMaxValeur(){
        String val = this.tfValMaxQuestion.getText();
        return Integer.parseInt(val);
    }

    public void setMaxValeurQ(int i){
        this.tfValMaxQuestion.setText(i+"");
    }

    public List<String> getReponses() throws PasDeReponsesQuestionException{
        Character type = this.modeleSondages.getSondageActuel().getQuestionActuelle().getTypeR();
        if((this.modeleSondages.getSondageActuel().getQuestionActuelle().getPropositions().size()==0 &&
        (type == 'c' || type == 'm' || type == 'u')) || (type == 'n' && this.modeleSondages.getSondageActuel().getQuestionActuelle().getMaxValeur() > 0)){
            throw new PasDeReponsesQuestionException();
        }
        return this.modeleSondages.getSondageActuel().getQuestionActuelle().getPropositions();
    }
}
