package vue;
import modele.*;

import java.sql.SQLDataException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

public class ControleurConnexion implements EventHandler<ActionEvent> {
    private Allo45 allo45;
    private FenetreConnexion fenetreConnexion;
    private ModeleSondages listeSondages;
    
    public ControleurConnexion(Allo45 allo45, FenetreConnexion fenetreConnexion, ModeleSondages listeSondages) {
        this.allo45 = allo45;
        this.fenetreConnexion = fenetreConnexion;
        this.listeSondages = listeSondages;
    }

    @Override
    public void handle(ActionEvent event) {
        String identifiant = this.fenetreConnexion.getIdentifiant();
        String motDePasse = this.fenetreConnexion.getMotDePasse();
        try {
            Utilisateur user = this.listeSondages.getRequetes().obtenirUtilisateur(identifiant, motDePasse);
            if (user.getRole().equals("Concepteur")) {
                this.allo45.afficheFenetreConcepteur(user);
            }
            else if (user.getRole().equals("Sondeur")) {
                this.listeSondages.initSetUpSondeur(user);
                this.allo45.afficheFenetreSondeur(user);
            }
            else if (user.getRole().equals("Analyste")) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION,"Le module Analyste n'est pas encore disponible");
                alert.setTitle("Attention");
                alert.setHeaderText("Pas de module Analyste");
                alert.showAndWait();
                this.fenetreConnexion.reset();
            }
        } catch(SQLDataException e){
            System.out.println("Votre identifiant ou mot de passe n'est pas valide");
        } catch(SQLException e){
            System.out.println("requete non valide");
        }
    }
}