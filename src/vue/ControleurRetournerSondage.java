package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetournerSondage implements EventHandler<ActionEvent> {

    private FenetreConcepteur appli;

    public ControleurRetournerSondage(FenetreConcepteur appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.afficheSondage();    
    }
    
}
