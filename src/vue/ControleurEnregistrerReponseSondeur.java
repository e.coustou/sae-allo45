package vue;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import modele.ModeleSondages;
import modele.Reponse;


public class ControleurEnregistrerReponseSondeur implements EventHandler<ActionEvent> {
    private FenetreSondeur vue;
    private ModeleSondages modele;

    public  ControleurEnregistrerReponseSondeur(FenetreSondeur vue,ModeleSondages modele){
        this.vue = vue;
        this.modele = modele;
    }
    @Override
    public void handle(ActionEvent event) {
        try {
            ComboBox<Integer> reponses = (ComboBox<Integer>) event.getSource();
            String rep = String.valueOf(reponses.getValue());
            Reponse reponse = new Reponse(rep, this.modele.getSondageActuel().getNumero(), this.modele.getSondageActuel().getQuestionActuelle().getNumero(), this.modele.getSondageActuel().getPanel().getSondeActuel().getNumero());
            this.modele.getRequetes().enregistrerReponse(reponse);
            }
        catch (SQLException e) {
            System.out.println("Impossible d'enregistrer la réponse");
        }

    }
}