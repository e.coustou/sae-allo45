package vue;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import modele.ModeleSondages;

public class ControleurAjoutChoix implements EventHandler<MouseEvent>{
    private ModeleSondages modele;

    public ControleurAjoutChoix(ModeleSondages modele){
        this.modele = modele;
    }

    @Override
    public void handle(MouseEvent event) {
        TextField tf = (TextField)event.getSource();
        this.modele.getSondageActuel().getQuestionActuelle().ajouteProposition(tf.getText());
    }
}
