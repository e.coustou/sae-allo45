package vue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurQuitter implements EventHandler<ActionEvent> {
    private Allo45 allo45;
    
    public ControleurQuitter(Allo45 allo45) {
        this.allo45 = allo45;
    }

    @Override
    public void handle(ActionEvent event) {
        this.allo45.quitte();
    }
}
