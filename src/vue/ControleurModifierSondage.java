package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.ModeleSondages;
import modele.Sondage;

public class ControleurModifierSondage implements EventHandler<ActionEvent> {
    /**
     * C'est la question que l'on veut modifier
     */
    private Sondage sondage;
    /**
     * C'est le Sondage de la question
     */
    private ModeleSondages modele;
    /**
     * C'est la vue ou sont les méthodes d'affichage
     */
    private FenetreConcepteur appli;


    public ControleurModifierSondage(FenetreConcepteur appli, Sondage sondage, ModeleSondages modele){
        this.appli = appli;
        this.modele = modele;
        this.sondage = sondage;
    }

    @Override
    public void handle(ActionEvent event) {
        this.modele.setSondageActuel(sondage);
        this.appli.afficheSondage();
    }
}
