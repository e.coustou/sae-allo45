package vue;

import java.sql.SQLException;

import exceptions.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.*;

public class ControleurEnregistrerSondage implements EventHandler<ActionEvent> {
    
    private ModeleSondages modele;
    private FenetreConcepteur appli;

    public ControleurEnregistrerSondage(ModeleSondages modele, FenetreConcepteur appli){
        this.modele = modele;
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event){

        
        Sondage sondage = this.modele.getSondageActuel();

        try{
            sondage.setTitre(this.appli.getTitreS());
        }
        catch(PasDeTitreSondageException e){
            System.out.println("Pas de titre dans le sondage");
        }

        try{
            sondage.setClient(this.appli.getClientS());
        }
        catch(PasDeClientSondageException e){
            System.out.println("Pas de client dans le sondage");
        }

        try{
            sondage.setPanel(this.appli.getPanelS());
        }
        catch(PasDePanelSondageException e){
            System.out.println("Pas de panel dans le sondage");
        }

        sondage.setEtat('C');

        try {
            this.modele.getRequetes().ajouterSondage(sondage);
        } catch (SQLException e) {
            System.out.println("Ajout du sondage dans la base de données a échoué");
        }
        this.appli.afficheAccueil();
    }

}