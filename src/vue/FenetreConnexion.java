package vue;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import modele.ModeleSondages;

public class FenetreConnexion extends BorderPane {
    private Allo45 allo45;
    private TextField TFIdentifiant;
    private PasswordField PFMotDePasse;
    private ModeleSondages listeSondages;

    public FenetreConnexion(Allo45 allo45, ModeleSondages listeSondages) {
        this.allo45 = allo45;
        this.listeSondages = listeSondages;
        this.ajouteTitle();
        this.ajouteGridPane();
    }

    public void ajouteTitle() {
        Label title = new Label("Bienvenue  sur  Allo45");
        title.setStyle("-fx-font-size: 40px; -fx-font-weight: bold; -fx-background-color: #A9A9A9; -fx-padding: 30px;");
        BorderPane.setAlignment(title, Pos.CENTER);

        this.setTop(title);
    }

    public void ajouteGridPane() {
        GridPane gridPane = new GridPane();

        Label identifiant = new Label("Identifiant : ");
        identifiant.setStyle("-fx-font-size: 20px;");
        Label motDePasse = new Label("Mot de passe : ");
        motDePasse.setStyle("-fx-font-size: 20px;");
        this.TFIdentifiant = new TextField();
        this.TFIdentifiant.setStyle("-fx-font-size: 15px");
        this.PFMotDePasse = new PasswordField();
        this.PFMotDePasse.setStyle("-fx-font-size: 15px");

        Button boutonConnexion = new Button("Connexion") {{
            setStyle("-fx-color: #0000FF; -fx-font-size: 15;");
            setOnMouseEntered(e -> setStyle("-fx-color: #0060FF; -fx-font-size: 15;"));
            setOnMouseExited(e -> setStyle("-fx-color: #0000FF; -fx-font-size: 15;"));
        }};
        boutonConnexion.setOnAction(new ControleurConnexion(this.allo45, this, this.listeSondages));

        Button boutonQuitter = new Button("Quitter") {{
            setStyle("-fx-color: #FF0000; -fx-font-size: 15;");
            setOnMouseEntered(e -> setStyle("-fx-color: #FF4000; -fx-font-size: 15;"));
            setOnMouseExited(e -> setStyle("-fx-color: #FF0000; -fx-font-size: 15;"));
        }};
        boutonQuitter.setOnAction(new ControleurQuitter(this.allo45));

        gridPane.add(identifiant, 0, 0);
        gridPane.add(motDePasse, 0, 1);
        gridPane.add(TFIdentifiant, 1, 0);
        gridPane.add(PFMotDePasse, 1, 1);
        gridPane.add(boutonQuitter, 0, 3);
        GridPane.setHalignment(boutonQuitter, HPos.CENTER);
        gridPane.add(boutonConnexion, 1, 3);
        GridPane.setHalignment(boutonConnexion, HPos.CENTER);

        gridPane.setStyle("-fx-alignment: center; -fx-padding: 50px; -fx-hgap: 40; -fx-vgap: 40;"+
        "-fx-border-color: #000000; -fx-border-insets: 50 30 30 30; -fx-border-radius: 10px; -fx-border-width: 3px;"+
        "-fx-background-color: #AAAAAA; -fx-background-radius: 10px; -fx-background-insets: 50 30 30 30;");

        this.setCenter(gridPane);
    }

    public String getIdentifiant() {
        return this.TFIdentifiant.getText();
    }

    public String getMotDePasse() {
        return this.PFMotDePasse.getText();
    }

    public void reset() {
        this.TFIdentifiant.clear();
        this.PFMotDePasse.clear();
    }
}
