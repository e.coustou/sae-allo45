package vue;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {
    private Allo45 allo45;

    public ControleurDeconnexion(Allo45 allo45) {
        this.allo45 = allo45;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Optional<ButtonType> reponse = this.allo45.popupDeconnexion().showAndWait();
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            this.allo45.afficheFenetreConnexion();
        }
        
    }
}
