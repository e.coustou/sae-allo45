package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.Sondage;

public class ControleurQuestionPrecedente implements EventHandler<ActionEvent> {
    private Sondage sondage;
    private FenetreSondeur vue;
    
    public ControleurQuestionPrecedente(Sondage sondage, FenetreSondeur vue){
        this.sondage = sondage;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.sondage.questionPrecedente();
        this.vue.majafficheFenetreSondeur();
    }
}

