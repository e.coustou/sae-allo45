package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.ModeleSondages;
import modele.Sondage;

public class ControleurValiderSondage implements EventHandler<ActionEvent> {
    private ModeleSondages modele;
    private Sondage sondage;
    private FenetreConcepteur fenetre;

    public ControleurValiderSondage(ModeleSondages modele, FenetreConcepteur fenetre, Sondage sondage){
        this.modele = modele;
        this.sondage = sondage;
        this.fenetre = fenetre;
    }

    @Override
    public void handle(ActionEvent event) {
        Sondage s = this.modele.getSondageActuel();
        if (s.getQuestions().size() != 0 && !(s.getClient() == null) && !(s.getTitre() == null) && s.getQuestions().size() != 0) {
            this.modele.validerSondage(this.sondage);
            this.fenetre.afficheAccueil();
        }
    }
    
    
}
