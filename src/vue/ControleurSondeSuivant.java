package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.ModeleSondages;
import modele.Utilisateur;

public class ControleurSondeSuivant implements EventHandler<ActionEvent> {
    private ModeleSondages modele;
    private FenetreSondeur vue;
    private Utilisateur sondeur;

    public ControleurSondeSuivant(ModeleSondages modele, FenetreSondeur vue, Utilisateur sondeur){
        this.modele = modele;
        this.vue = vue;
        this.sondeur = sondeur;
    }
    @Override
    public void handle(ActionEvent actionEvent) {
        this.modele.initSetUpSondeur(this.sondeur);
        this.vue.majafficheFenetreSondeur();
    }
}
    

