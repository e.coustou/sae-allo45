package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.Sondage;

public class ControleurQuestionSuivante implements EventHandler<ActionEvent>{
    private Sondage sondage;
    private FenetreSondeur vue;

    public ControleurQuestionSuivante(Sondage sondage, FenetreSondeur vue){
        this.sondage = sondage;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.sondage.questionSuivante();
        this.vue.majafficheFenetreSondeur();
    }
}