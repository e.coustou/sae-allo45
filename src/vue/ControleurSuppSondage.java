package vue;
import modele.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurSuppSondage implements EventHandler<ActionEvent> {
    
    private ModeleSondages modele;
    private Sondage sondage;
    private FenetreConcepteur vue;

    public ControleurSuppSondage(ModeleSondages modele, Sondage sondage, FenetreConcepteur vue){
        this.modele = modele;
        this.sondage = sondage;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent event){
        this.modele.supprimerSondage(this.sondage);
        this.vue.afficheAccueil();
    }
}