package vue;
import modele.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerChoix implements EventHandler<ActionEvent>{
    private ModeleSondages modele;
    private FenetreConcepteur vue;

    public ControleurCreerChoix(ModeleSondages modele, FenetreConcepteur vue) {
        this.modele = modele;
        this.vue = vue;
    }
    @Override
    public void handle(ActionEvent event) {
        this.modele.ajoutProposition();
        this.vue.afficheQuestion();
    }
}