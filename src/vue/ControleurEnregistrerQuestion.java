package vue;

import exceptions.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.*;

public class ControleurEnregistrerQuestion implements EventHandler<ActionEvent> {
    
    private Sondage sondage;
    private FenetreConcepteur appli;

    public ControleurEnregistrerQuestion(Sondage sondage, FenetreConcepteur appli){
        this.sondage = sondage;
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event){

           
        Question question = this.sondage.getQuestionActuelle();
  
        try{
            question.setTitre(this.appli.getTitreQ());
        }
        catch(PasDeTitreQuestionException e){
            System.out.println("Pas de titre dans la question");
        }

        question.setTypeR(this.appli.getTypeR());

        if(!(question.getTypeR() =='u' || question.getTypeR() =='l')){
            question.setMaxValeur(this.appli.getMaxValeur());
        }

        try{
            for(String reponse: this.appli.getReponses()){
                question.ajouteProposition(reponse);
            }
        }
        catch(PasDeReponsesQuestionException e){
            System.out.println("Pas de propositions dans la question");
        }

        this.sondage.ajouterQuestion(question);
    }

}