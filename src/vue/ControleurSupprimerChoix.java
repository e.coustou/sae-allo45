package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.ModeleSondages;

public class ControleurSupprimerChoix implements EventHandler<ActionEvent> {
    private ModeleSondages modele;
    private String choix;
    private FenetreConcepteur vue;

    public ControleurSupprimerChoix(ModeleSondages modele, String choix, FenetreConcepteur vue){
        this.modele = modele;
        this.choix = choix;
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        for(int i = 0; i < modele.getSondageActuel().getQuestionActuelle().getPropositions().size();i++){
            if(modele.getSondageActuel().getQuestionActuelle().getPropositions().get(i).equals(choix)){
                modele.getSondageActuel().getQuestionActuelle().getPropositions().remove(i);
                break;
            }
        }
        this.vue.afficheQuestion();
    }
    
}
